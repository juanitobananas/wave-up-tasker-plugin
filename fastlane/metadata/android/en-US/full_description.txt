A Tasker[1] plugin to allow to enable and disable WaveUp using Tasker. For the nerdier 😉

This Tasker plugin is based on Termux:Task and is also released under the GPLv3 license[2]. Of course, it's also open source and you can check out the code[3].

@Termux Team[4]: Thank you very much for this cool piece of code!

How to use

★ Create a new Tasker Action.
★ In the resulting Select Action Category dialog, select Plugin.
★ In the resulting Action Plugin dialog, select WaveUp.
★ Edit the configuration to specify whether WaveUp should be enabled or disabled.

[1] https://play.google.com/store/apps/details?id=net.dinglisch.android.taskerm
[2] https://www.gnu.org/licenses/gpl-3.0.en.html
[3] https://gitlab.com/juanitobananas/wave-up-tasker-plugin
[4] https://play.google.com/store/apps/details?id=com.termux
