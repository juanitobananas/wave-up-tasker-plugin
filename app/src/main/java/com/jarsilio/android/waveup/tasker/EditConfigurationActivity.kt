package com.jarsilio.android.waveup.tasker

import android.content.Intent
import android.os.Bundle
import android.widget.RadioGroup
import com.jarsilio.android.waveup.tasker.PluginBundleManager.EXTRA_ENABLE
import com.twofortyfouram.locale.api.Intent.EXTRA_BUNDLE
import com.twofortyfouram.locale.api.Intent.EXTRA_STRING_BLURB
import java.io.File

/**
 * This is the "Edit" activity for a Locale Plug-in.
 *
 *
 * This Activity can be started in one of two states:
 *
 *  * New plug-in: The Activity's Intent will not contain "com.twofortyfouram.locale.Intent#EXTRA_BUNDLE".
 *  * Old plug-in: The Activity's Intent will contain "com.twofortyfouram.locale.Intent#EXTRA_BUNDLE" from
 * a previously saved plug-in instance that the user is editing.
 *
 */
class EditConfigurationActivity : AbstractPluginActivity() {
    private val radioGroup by lazy { findViewById<RadioGroup>(R.id.radio_button) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val actionBar = supportActionBar
        actionBar?.setTitle(R.string.app_name)
        actionBar?.setDisplayHomeAsUpEnabled(true)

        setContentView(R.layout.edit_activity)

        val intent = intent
        BundleScrubber.scrub(intent)
        val localeBundle = intent.getBundleExtra(EXTRA_BUNDLE)
        BundleScrubber.scrub(localeBundle)

        if (PluginBundleManager.isBundleValid(localeBundle) &&
            localeBundle!!.getBoolean(EXTRA_ENABLE)
        ) {
            radioGroup.check(R.id.enable_radio_button)
        } else {
            radioGroup.check(R.id.disable_radio_button)
        }
    }

    override fun finish() {
        if (!isCanceled) {
            val resultIntent = Intent()

            /*
             * This extra is the data to ourselves: either for the Activity or the BroadcastReceiver. Note
             * that anything placed in this Bundle must be available to Locale's class loader. So storing
             * String, int, and other standard objects will work just fine. Parcelable objects are not
             * acceptable, unless they also implement Serializable. Serializable objects must be standard
             * Android platform objects (A Serializable class private to this plug-in's APK cannot be
             * stored in the Bundle, as Locale's classloader will not recognize it).
             */
            val enable =
                when (radioGroup.checkedRadioButtonId) {
                    R.id.enable_radio_button -> true
                    R.id.disable_radio_button -> false
                    else -> true
                }

            val resultBundle = PluginBundleManager.generateBundle(applicationContext, enable)

            // The blurb is a concise status text to be displayed in the host's UI.
            val blurb = generateBlurb(enable)
            if (TaskerPlugin.Setting.hostSupportsOnFireVariableReplacement(this)) {
                TaskerPlugin.Setting.setVariableReplaceKeys(resultBundle, arrayOf(PluginBundleManager.EXTRA_ENABLE))
            }

            resultIntent.putExtra(EXTRA_BUNDLE, resultBundle)
            resultIntent.putExtra(EXTRA_STRING_BLURB, blurb)
            setResult(RESULT_OK, resultIntent)
        }

        super.finish()
    }

    /**
     * @param executable The toast message to be displayed by the plug-in. Cannot be null.
     * @return A blurb for the plug-in.
     */
    internal fun generateBlurb(enable: Boolean): String {
        val message =
            if (enable) {
                "Enable WaveUp"
            } else {
                "Disable WaveUp"
            }

        val maxBlurbLength = 60 // R.integer.twofortyfouram_locale_maximum_blurb_length.
        return if (message.length > maxBlurbLength) {
            message.substring(0, maxBlurbLength)
        } else {
            message
        }
    }

    companion object {
        val TASKER_DIR = File("/data/data/com.termux/files/home/.termux/tasker/")
    }
}
