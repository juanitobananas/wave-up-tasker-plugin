# WaveUp Tasker Plugin

A [Tasker](https://tasker.joaoapps.com/) plugin to allow to enable and disable WaveUp using Tasker. For the nerdier :wink:

This Tasker plugin is based on [Termux:Task](https://github.com/termux/termux-tasker) and is also released under the [GPLv3 license](https://www.gnu.org/licenses/gpl.html).

[@Termux Team](https://github.com/termux): Thank you very much for this cool piece of code!

## How to use

1. Create a new Tasker Action.
2. In the resulting *Select Action Category* dialog, select *Plugin*.
3. In the resulting *Action Plugin* dialog, select *WaveUp*.
4. Edit the configuration to specify whether WaveUp should be enabled or disabled.


[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="80">](https://f-droid.org/packages/com.jarsilio.android.waveup.tasker/)
[<img src="https://play.google.com/intl/en_us/badges/images/generic/en-play-badge.png"
     alt="Get it on Google Play"
     height="80">](https://play.google.com/store/apps/details?id=com.jarsilio.android.waveup.tasker)