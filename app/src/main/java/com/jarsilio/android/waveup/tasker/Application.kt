package com.jarsilio.android.waveup.tasker

import android.app.Application
import com.jarsilio.android.common.logging.LongTagTree
import timber.log.Timber

@Suppress("unused")
class Application : Application() {
    override fun onCreate() {
        super.onCreate()
        Timber.plant(LongTagTree(this))
    }
}
