package com.jarsilio.android.waveup.tasker

import android.content.BroadcastReceiver
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import com.twofortyfouram.locale.api.Intent.ACTION_FIRE_SETTING
import com.twofortyfouram.locale.api.Intent.EXTRA_BUNDLE
import timber.log.Timber

/**
 * This is the "fire" BroadcastReceiver for a Locale Plug-in.
 */
class FireReceiver : BroadcastReceiver() {
    override fun onReceive(
        context: Context,
        intent: Intent,
    ) {
        if (ACTION_FIRE_SETTING != intent.action) {
            Timber.e("Unexpected intent action: ${intent.action}")
            return
        }

        Timber.d("Received fire setting. Starting WaveUp Tasker plug-in.")

        BundleScrubber.scrub(intent)
        val bundle = intent.getBundleExtra(EXTRA_BUNDLE)
        BundleScrubber.scrub(bundle)

        if (!PluginBundleManager.isBundleValid(bundle)) return
        val enable = bundle!!.getBoolean(PluginBundleManager.EXTRA_ENABLE)
        Timber.d("Configuration (EXTRA_ENABLE): $enable")
        val enableIntent =
            if (enable) {
                Timber.d("Sending Intent to enable WaveUpService")
                Intent("com.jarsilio.android.waveup.intent.action.WAVEUP_ENABLE")
            } else {
                Timber.d("Sending Intent to disable WaveUpService")
                Intent("com.jarsilio.android.waveup.intent.action.WAVEUP_DISABLE")
            }

        val infos = context.packageManager.queryBroadcastReceivers(enableIntent, 0)
        if (infos.isEmpty()) {
            Timber.e("WaveUp isn't installed or the version isn't compatible")
        }

        for (info in infos) {
            val componentName =
                ComponentName(
                    info.activityInfo.packageName,
                    info.activityInfo.name,
                )
            enableIntent.component = componentName
            context.sendBroadcast(enableIntent)
        }
    }
}
