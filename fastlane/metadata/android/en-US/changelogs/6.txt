New in version 0.1.5

★ Upgrade some dependencies.
★ Bump Android SDK for Google Play Store compatibility. I am not sure will work at all, as I can't test it. I guess we'll see :)

New in version 0.1.3

★ Upgrade some dependencies.
★ Bump Android SDK for Google Play Store compatibility.
